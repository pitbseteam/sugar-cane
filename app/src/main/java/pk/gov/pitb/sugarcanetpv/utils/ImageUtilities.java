package pk.gov.pitb.sugarcanetpv.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;


public class ImageUtilities {

    public static byte[] getSimpleBytes(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static Bitmap getImage(byte[] image)
    {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }


    public static String encodeToBase64String(byte[] data) {
        if (data == null || data.length == 0) {
            return "";
        } else {

            return Base64.encodeToString(data, Base64.DEFAULT);

        }

    }
public static byte[] decodeBase64ToBytes(String data) {
        if (data == null || data.length() == 0) {
            return null;
        } else {

            return Base64.decode(data, Base64.DEFAULT);

        }

    }


    public byte[] getBytes(Bitmap bitmap) {
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, true);// createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm", Locale.ENGLISH);
        String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system

        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(27);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Paint.Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200, resizedBitmap.getHeight() - 10, tPaint);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public static byte[] getBytes(Bitmap resizedBitmap, String picturePath) {
        File file = new File(picturePath);
        // bitmap=BitmapFactory.decodeFile(picturePath);

        // Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm", Locale.ENGLISH);
        // String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
        String dateTime = sdf.format(file.lastModified());
        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(27);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Paint.Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200, resizedBitmap.getHeight() - 10, tPaint);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    @SuppressLint("InflateParams")
    public static void previewPicture(Bitmap photo, String title) {
        final Dialog pictureViewDialog = new Dialog(Globals.getUsage().mContext);
        pictureViewDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        pictureViewDialog.setTitle(title);
        LayoutInflater li = (LayoutInflater) Globals.getUsage().mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = li.inflate(R.layout.dialog_preview_image_cross, null);
        pictureViewDialog.setContentView(dialogView);
        pictureViewDialog.show();
        ImageView previewPicture = (ImageView) dialogView.findViewById(R.id.previewpicture);
        ImageView imageViewClose = (ImageView) dialogView.findViewById(R.id.iv_close);

        previewPicture.setImageBitmap(photo);
//        previewPicture.setAdjustViewBounds(true);
//        previewPicture.setScaleType(ImageView.ScaleType.FIT_CENTER);


        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureViewDialog.dismiss();
            }
        });

    }
    @SuppressLint("InflateParams")
    public static void previewPicture(String imagePath, String title) {
        final Dialog pictureViewDialog = new Dialog(Globals.getUsage().mContext);
        pictureViewDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        pictureViewDialog.setTitle(title);
        LayoutInflater li = (LayoutInflater) Globals.getUsage().mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = li.inflate(R.layout.dialog_preview_image_cross, null);
        pictureViewDialog.setContentView(dialogView);
        pictureViewDialog.show();
        ImageView previewPicture = (ImageView) dialogView.findViewById(R.id.previewpicture);
        ImageView imageViewClose = (ImageView) dialogView.findViewById(R.id.iv_close);

        previewPicture.setImageBitmap(BitmapFactory.decodeFile(imagePath));


//        previewPicture.setImageBitmap(photo);
//        previewPicture.setAdjustViewBounds(true);
//        previewPicture.setScaleType(ImageView.ScaleType.FIT_CENTER);

        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureViewDialog.dismiss();
            }
        });
    }

    public static int getCameraPhotoOrientation(File imageFile) throws Exception {
        int rotate = 0;
        try {
            if(imageFile.exists()){
                ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
                int orientation = Integer.parseInt(exif.getAttribute(ExifInterface.TAG_ORIENTATION));
                switch (orientation) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        rotate = 0;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static Intent getVideoIntent() {
        Intent intent = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            intent = new Intent();
            intent.setType("video/*"); // intent.setType("file/*"); //
            // intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("video/*");
        }
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        // Intent.createChooser(intent, "Select Video")
        return intent;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static ImageView setVideoOnActivityResultCamera(Intent data, ImageView imageView, String videoDetails) throws Exception {
        Uri originalUri = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            originalUri = data.getData();
        } else {
            originalUri = data.getData();
            int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION) & data.getFlags();
            Globals.getUsage().mContext.getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
        }
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(ImageUtilities.getRealPathFromURI(originalUri),
                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setPadding(0, 0, 0, 0);
        imageView.setImageBitmap(bitmap);
        videoDetails = getRealPathFromURI(originalUri);
        return imageView;
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String setVideoOnActivityResultCamera(int requestCode, int resultCode, Intent data, ImageView imageView, String dataVideo)
            throws Exception {
        Uri originalUri = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            originalUri = data.getData();
        } else {
            originalUri = data.getData();
            int takeFlags = data.getFlags() & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            Globals.getUsage().mContext.getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
        }
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(ImageUtilities.getRealPathFromURI(originalUri),
                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        // imageView.setScaleType(ScaleType.FIT_XY);
        // imageView.setPadding(0, 0, 0, 0);
        // imageView.setImageBitmap(bitmap);

//        Drawable drawableOn = Globals.getUsage().mContext.getResources().getDrawable(R.drawable.camera_orange);
//        Drawable drawableOff = Globals.getUsage().mContext.getResources().getDrawable(R.drawable.camera_orange);
//        StateListDrawable statesButton1 = new StateListDrawable();
//        statesButton1.addState(new int[] { android.R.attr.state_pressed }, drawableOff);
//        statesButton1.addState(new int[] {}, drawableOn);
//        imageView.setBackgroundDrawable(statesButton1);

        dataVideo = getRealPathFromURI(originalUri);
//		videoDetails.data = originalUri.getPath();

        return dataVideo;
    }

//    public static String getRealPathFromURI(Uri contentUri) {
//        String[] proj = { MediaStore.Audio.Media.DATA };
//        Cursor cursor = Globals.getUsage().mContext.getContentResolver().query(contentUri, proj, null, null, null);
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
//        cursor.moveToFirst();
//        return cursor.getString(column_index);
//    }






///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // picture config


//    @Override
//    public void onConfigurationChanged (Configuration newConfig){
//        super.onConfigurationChanged(newConfig);
//    }

    /**
     * This method is responsible for solving the rotation issue if exist. Also scale the images to
     * 1024x1024 resolution
     *
     * @param context       The current context
     * @param selectedImage The Image URI
     * @return Bitmap image results
     * @throws IOException
     */

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(img, selectedImage);
        return img;
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run through a decode*
     *                  method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public static Bitmap getCompressedBitmap(String imagePath) {
//        float maxHeight = 1920.0f;
//        float maxWidth = 1080.0f;
        float maxHeight = 768.0f;
        float maxWidth = 1024.0f;

//        float maxHeight = 768.0f;
//        float maxWidth = 1024.0f;

        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        byte[] byteArray = out.toByteArray();

        Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return updatedBitmap;
    }

    //////////////////////////////////////////////////////////////////////////
    File pictureFile = null;
    private void storeImage(Bitmap image) {
        pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("permission issue",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("file not found", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("error access", "Error accessing file: " + e.getMessage());
        }
    }
    /** Create a File for saving an image or video */
    public File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + Globals.getUsage().mContext.getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    ///////////////////////////////////////////////////////

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title" + Globals.getDateTime(), null);
        return Uri.parse(path);
    }
    public static String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = Globals.getUsage().mActivity.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


}
