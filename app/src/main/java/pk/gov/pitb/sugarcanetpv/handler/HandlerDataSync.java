package pk.gov.pitb.sugarcanetpv.handler;


public abstract class HandlerDataSync {
	public abstract void onSyncSuccessful(boolean success);
}
