package pk.gov.pitb.sugarcanetpv.Ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import locationprovider.davidserrano.com.LocationProvider;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.dialogues.SweetAlertDialogs;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.SaveShipmentResponse;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.Shipment;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SugarMill;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pk.gov.pitb.sugarcanetpv.SplashScreen.siteKey;

public class ShipmentFormActivity extends AppCompatActivity {

    @BindView(R.id.et_purchased)
    EditText editTextPurchased;
    @BindView(R.id.et_crushed)
    EditText editTextCrushed;
    @BindView(R.id.et_produced)
    EditText editTextProduced;
    @BindView(R.id.et_sold)
    EditText editTextSold;
    @BindView(R.id.et_not_lifted)
    EditText editTextNotLifted;
    @BindView(R.id.et_stock)
    EditText editTextStock;

    @BindView(R.id.btn_submit)
    TextView btnSubmit;

    Intent intent;
    LocationProvider locationProvider;
    public String slat = "";
    public String slon = "";

    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;

    UserData userData;
    private Shipment classShipmentsForm;
    private SugarMill sugarMill;

    private void setInstances() {
        mGlobals = Globals.getInstance(this);
        SugarContext.init(this);
        ButterKnife.bind(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipment_form);
        setInstances();
        userData = UserData.first(UserData.class);
        sugarMill =(SugarMill) this.getIntent().getSerializableExtra("sugarmill");

        generateBody();
        getLocation();
        submitClickListner();

    }

    private void submitClickListner() {
        btnSubmit.setOnClickListener(v -> {
            if (validateFields()) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ShipmentFormActivity.this);
                dialog.setTitle("Submit | درج")
                        .setMessage(Globals.getUsage().mContext.getResources().getString(R.string.ask_submit))
                        .setPositiveButton("Yes - جی ہاں", (dialoginterface, i) -> {
                            saveShipment();
                            dialoginterface.dismiss();
                        })
                        .setNegativeButton("No - نہیں", (dialoginterface, i) -> dialoginterface.cancel())
                        .show();
            }

        });
    }

    public void generateBody() {


    }

    private boolean validateFields() {
        classShipmentsForm = new Shipment();

        String stringPurchased = editTextPurchased.getText().toString();
        String stringCrushed = editTextCrushed.getText().toString();
        String stringProduced = editTextProduced.getText().toString();
        String stringSold = editTextSold.getText().toString();
        String stringNotLifted = editTextNotLifted.getText().toString();
        String stringStock = editTextStock.getText().toString();


        if (stringPurchased.length() == 0) {
            Toast.makeText(this, "Enter sugar purchased", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (stringCrushed.length() == 0) {
            Toast.makeText(this, "Enter sugar crushed", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (stringProduced.length() == 0) {
            Toast.makeText(this, "Enter sugar produced", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (stringSold.length() == 0) {
            Toast.makeText(this, "Enter sugar sold", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (stringNotLifted.length() == 0) {
            Toast.makeText(this, "Enter sugar not lifted", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (stringStock.length() == 0) {
            Toast.makeText(this, "Enter sugar stock", Toast.LENGTH_SHORT).show();
            return false;
        }


        classShipmentsForm.setUserId(userData.getUserId().toString());
        classShipmentsForm.setMillIdFk(sugarMill.getSmId());
        classShipmentsForm.setMillName(sugarMill.getSmName());

        classShipmentsForm.setStdPurchased(Float.parseFloat(stringPurchased));
        classShipmentsForm.setStdCrushed(Float.parseFloat(stringCrushed));
        classShipmentsForm.setStdSugarProduced(Float.parseFloat(stringProduced));
        classShipmentsForm.setStdSugarSold(Float.parseFloat(stringSold));
        classShipmentsForm.setStdSugarNotLifted(Float.parseFloat(stringNotLifted));
        classShipmentsForm.setStdSugarStocked(Float.parseFloat(stringStock));

        //location
        classShipmentsForm.setmSsLatLong(slat + "," + slon);
        classShipmentsForm.setmSsActivityDatetime(Globals.getDateTime());

        return true;
    }

    @Override
    protected void onResume() {
        if (locationProvider != null)
            locationProvider.requestLocation();

        super.onResume();

        if (!Globals.getUsage().isTimeAutomatic(this)) {
            Toast.makeText(Globals.getUsage().mContext, "Please Turn on automatic time", Toast.LENGTH_LONG).show();
            Intent i = new Intent(Settings.ACTION_DATE_SETTINGS);
            Globals.getUsage().mContext.startActivity(i);
        } else if (Globals.getUsage().isAirplaneModeOn(this)) {
            Toast.makeText(Globals.getUsage().mContext, "Please Turn off airplane mode", Toast.LENGTH_LONG).show();
            Intent i = new Intent(Settings.ACTION_DATE_SETTINGS);
            Globals.getUsage().mContext.startActivity(i);
        }
        super.onResume();
    }

    private void saveShipment() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait... | انتظار کریں");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Gson gson = new Gson();
        String jsonString = gson.toJson(classShipmentsForm);
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(jsonString);
        JsonObject jsonObject = json.getAsJsonObject();


        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());
        Log.e("json", jsonObject.toString());


        RequestBody requestBodyData = RequestBody.create(MediaType.parse("text/plain"),jsonObject.toString());
        RequestBody requestBodyAppData = RequestBody.create(MediaType.parse("text/plain"),Globals.getUsage().getJSON().toString());

            mAPIService.saveShipment(siteKey(), requestBodyData,requestBodyAppData)
                    .enqueue(new Callback<SaveShipmentResponse>() {
                        //        mAPIService.saveFarmer(hashMap).enqueue(new Callback<GeneralResponse>() {
                        @Override
                        public void onResponse(Call<SaveShipmentResponse> call, Response<SaveShipmentResponse> response) {
                            onResponseCase(classShipmentsForm, response);
                        }

                        @Override
                        public void onFailure(Call<SaveShipmentResponse> call, Throwable t) {

                            onFailureCase(t);
                        }
                    });



    }

    public void onResponseCase(Shipment classShipmentsForm, Response<SaveShipmentResponse> response){
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            Log.i("TAG", "post submitted to API." + response.body().toString());

            SaveShipmentResponse loginResponse = response.body();

            if (loginResponse.getStatus()) {
                // success
                Shipment shipment = response.body().getData();
                classShipmentsForm.setIsunsent("false");
                classShipmentsForm.save();

//                                classShipmentsForm.save();
                SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Registered | رجسٹرڈ", loginResponse.getMessage(), new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                }, false);
            } else {
//                                classShipmentsForm.setIsunsent("true");
//                                classShipmentsForm.save();

                SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Cannot Submit", loginResponse.getMessage(), new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
//                                        finish();
                    }
                }, false);
            }
        } else {
            classShipmentsForm.setIsunsent("true");
            classShipmentsForm.save();
//                            Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.error_data_saved));
            SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Cannot Submit", Globals.getUsage().mContext.getString(R.string.error_data_saved), new SweetAlertDialogs.OnDialogClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    finish();
                }
            }, false);
        }
    }

  public void onFailureCase(Throwable t){
      progressDialog.dismiss();
      classShipmentsForm.setIsunsent("true");
      classShipmentsForm.save();
      Log.e("failure", "Unable to submit post to API.");
      if (t instanceof IOException) {
          SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Cannot Submit", Globals.getUsage().mContext.getString(R.string.error_data_saved), new SweetAlertDialogs.OnDialogClickListener() {
              @Override
              public void onClick(SweetAlertDialog sweetAlertDialog) {
                  finish();
              }
          }, false);
      } else {
          SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Cannot Submit", Globals.getUsage().mContext.getString(R.string.error_data_saved), new SweetAlertDialogs.OnDialogClickListener() {
              @Override
              public void onClick(SweetAlertDialog sweetAlertDialog) {
                  finish();
              }
          }, false);
      }
  }
    public void getLocation() {
        //create a callback
        LocationProvider.LocationCallback callback = new LocationProvider.LocationCallback() {


            @Override
            public void onNewLocationAvailable(float lat, float lon) {
                //location update
//                Log.e("location","lat: " + lat + " long: " + lon);
                slat = lat + "";
                slon = lon + "";
            }

            @Override
            public void locationServicesNotEnabled() {
                //failed finding a location
                Toast.makeText(Globals.getUsage().mContext, Globals.getUsage().mContext.getResources().getString(R.string.location_on), Toast.LENGTH_LONG).show();
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                Globals.getUsage().mActivity.startActivity(i);

            }

            @Override
            public void updateLocationInBackground(float lat, float lon) {
                //if a listener returns after the main locationAvailable callback, it will go here
//                Log.e("location","update: lat: " + lat + " long: " + lon);
                slat = lat + "";
                slon = lon + "";
            }

            @Override
            public void networkListenerInitialised() {
                //when the library switched from GPS only to GPS & network
            }

            @Override
            public void locationRequestStopped() {
                //
            }
        };

        //initialise an instance with the two required parameters
        locationProvider = new LocationProvider.Builder()
                .setContext(this)
                .setListener(callback)
                .create();

        //start getting location
        locationProvider.requestLocation();
    }

    @Override
    public void onBackPressed() {
        alertViewExit();
    }

    private void alertViewExit() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Close Form? | فارم بند کریں")
                .setMessage(Globals.getUsage().mContext.getResources().getString(R.string.data_be_lost))
                .setPositiveButton("Yes - جی ہاں", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("No - نہیں", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                    }
                })
                .show();
    }
}