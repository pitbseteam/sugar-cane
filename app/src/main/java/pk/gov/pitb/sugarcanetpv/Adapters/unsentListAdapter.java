package pk.gov.pitb.sugarcanetpv.Adapters;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.Ui.ShipmentFormActivity;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.ClassFlourMill;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.Shipment;


public class unsentListAdapter extends RecyclerView.Adapter<unsentListAdapter.ViewHolder> {

    List<Shipment> mList;
    Context context;
    int index;
    UserData userData;

    public unsentListAdapter(List<Shipment> mList, Context context) {

        setHasStableIds(true);
        this.mList = mList;
        this.context = context;
        userData = UserData.first(UserData.class);
    }

    @Override
    public unsentListAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.unsent_items, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = holder.getAdapterPosition();
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(unsentListAdapter.ViewHolder viewHolder, int position) {
        index = position;
        viewHolder.textViewName.setText("Sugar Mill Name: "+mList.get(position).getMillName());
        viewHolder.textViewPurchased.setText("Purchased: "+mList.get(position).getStdPurchased());
        viewHolder.textViewCrushed.setText("Crushed: " + mList.get(position).getStdCrushed());
        viewHolder.textViewProduced.setText("Produced: " +mList.get(position).getStdSugarProduced());
        viewHolder.textViewSold.setText("Sold: "+ mList.get(position).getStdSugarSold());
        viewHolder.textViewNotLifted.setText("Not Lifted: "+mList.get(position).getStdSugarNotLifted());
        viewHolder.textViewStock.setText("Stocked: "+mList.get(position).getStdSugarStocked());

        viewHolder.linearLayoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Globals.getUsage().mContext, ShipmentFormActivity.class);
//                intent.putExtra("flourmill",mList.get(position));
//                Globals.getUsage().mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout linearLayoutMain;
        public TextView textViewName, textViewProduced, textViewCrushed, textViewPurchased, textViewSold, textViewNotLifted, textViewStock;
        public ImageView imageView;
//        public ProgressWheel progressWheel;

        public ViewHolder( View itemView) {
            super(itemView);

            linearLayoutMain = itemView.findViewById(R.id.ll_main);
            textViewName = itemView.findViewById(R.id.tv_sugar_mill_name);
            textViewProduced = itemView.findViewById(R.id.tv_produced);
            textViewCrushed = itemView.findViewById(R.id.tv_crushed);
            textViewPurchased = itemView.findViewById(R.id.tv_purchased);
            textViewSold = itemView.findViewById(R.id.tv_sold);
            textViewNotLifted = itemView.findViewById(R.id.tv_not_lifted);
            textViewStock = itemView.findViewById(R.id.tv_stock);
            imageView = itemView.findViewById(R.id.imageView);
//            progressWheel = itemView.findViewById(R.id.image_progress_wheel);
        }
    }

    public void updateList(List<Shipment> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
