package pk.gov.pitb.sugarcanetpv.models.generalResponse;

import com.google.gson.annotations.SerializedName;

public class RecaptchaResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String Message;
}
