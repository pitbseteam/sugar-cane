package pk.gov.pitb.sugarcanetpv.interfaces;

public interface AdapterCallback {
    void onMethodCallback(int position, String value);
}