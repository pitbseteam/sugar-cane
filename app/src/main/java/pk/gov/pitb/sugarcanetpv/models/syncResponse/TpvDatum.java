
package pk.gov.pitb.sugarcanetpv.models.syncResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class TpvDatum  extends SugarRecord implements Serializable {

    @SerializedName("std_id")
    @Expose
    private Integer stdId;
    @SerializedName("mill_idFk")
    @Expose
    private Integer millIdFk;
    @SerializedName("std_purchased")
    @Expose
    private Integer stdPurchased;
    @SerializedName("std_crushed")
    @Expose
    private Integer stdCrushed;
    @SerializedName("std_sugar_poduced")
    @Expose
    private Integer stdSugarPoduced;
    @SerializedName("std_sugar_sold")
    @Expose
    private Integer stdSugarSold;
    @SerializedName("std_sugar_not_lifted")
    @Expose
    private Integer stdSugarNotLifted;
    @SerializedName("std_sugar_stock")
    @Expose
    private Integer stdSugarStock;
    @SerializedName("std_activity_datetime")
    @Expose
    private String stdActivityDatetime;
    @SerializedName("std_lat_long")
    @Expose
    private String stdLatLong;
    @SerializedName("std_status")
    @Expose
    private Integer stdStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("updated_by")
    @Expose
    private Integer updatedBy;
    @SerializedName("sugar_mill_name")
    @Expose
    private String sugarMillName;

    public Integer getStdId() {
        return stdId;
    }

    public void setStdId(Integer stdId) {
        this.stdId = stdId;
    }

    public Integer getMillIdFk() {
        return millIdFk;
    }

    public void setMillIdFk(Integer millIdFk) {
        this.millIdFk = millIdFk;
    }

    public Integer getStdPurchased() {
        return stdPurchased;
    }

    public void setStdPurchased(Integer stdPurchased) {
        this.stdPurchased = stdPurchased;
    }

    public Integer getStdCrushed() {
        return stdCrushed;
    }

    public void setStdCrushed(Integer stdCrushed) {
        this.stdCrushed = stdCrushed;
    }

    public Integer getStdSugarPoduced() {
        return stdSugarPoduced;
    }

    public void setStdSugarPoduced(Integer stdSugarPoduced) {
        this.stdSugarPoduced = stdSugarPoduced;
    }

    public Integer getStdSugarSold() {
        return stdSugarSold;
    }

    public void setStdSugarSold(Integer stdSugarSold) {
        this.stdSugarSold = stdSugarSold;
    }

    public Integer getStdSugarNotLifted() {
        return stdSugarNotLifted;
    }

    public void setStdSugarNotLifted(Integer stdSugarNotLifted) {
        this.stdSugarNotLifted = stdSugarNotLifted;
    }

    public Integer getStdSugarStock() {
        return stdSugarStock;
    }

    public void setStdSugarStock(Integer stdSugarStock) {
        this.stdSugarStock = stdSugarStock;
    }

    public String getStdActivityDatetime() {
        return stdActivityDatetime;
    }

    public void setStdActivityDatetime(String stdActivityDatetime) {
        this.stdActivityDatetime = stdActivityDatetime;
    }

    public String getStdLatLong() {
        return stdLatLong;
    }

    public void setStdLatLong(String stdLatLong) {
        this.stdLatLong = stdLatLong;
    }

    public Integer getStdStatus() {
        return stdStatus;
    }

    public void setStdStatus(Integer stdStatus) {
        this.stdStatus = stdStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getSugarMillName() {
        return sugarMillName;
    }

    public void setSugarMillName(String sugarMillName) {
        this.sugarMillName = sugarMillName;
    }

}
