
package pk.gov.pitb.sugarcanetpv.models.loginResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class UserData extends SugarRecord {


    @SerializedName("imei_number")
    @Expose
    private Object imeiNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("cnic")
    @Expose
    private String cnic;
    @SerializedName("ownership")
    @Expose
    private Object ownership;
    @SerializedName("contact_number")
    @Expose
    private Object contactNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("division_idFk")
    @Expose
    private Integer divisionIdFk;
    @SerializedName("district_idFk")
    @Expose
    private Integer districtIdFk;
    @SerializedName("tehsil_idFk")
    @Expose
    private Object tehsilIdFk;
    @SerializedName("center_idFk")
    @Expose
    private Object centerIdFk;
    @SerializedName("mauza_idFk")
    @Expose
    private Object mauzaIdFk;
    @SerializedName("remember_token")
    @Expose
    private Object rememberToken;
    @SerializedName("app_user_token")
    @Expose
    private String appUserToken;
    @SerializedName("app_user_token_expiry")
    @Expose
    private Object appUserTokenExpiry;
    @SerializedName("profile_picture")
    @Expose
    private Object profilePicture;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("role_name")
    @Expose
    private String roleName;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Object getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(Object imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public Object getOwnership() {
        return ownership;
    }

    public void setOwnership(Object ownership) {
        this.ownership = ownership;
    }

    public Object getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(Object contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDivisionIdFk() {
        return divisionIdFk;
    }

    public void setDivisionIdFk(Integer divisionIdFk) {
        this.divisionIdFk = divisionIdFk;
    }

    public Integer getDistrictIdFk() {
        return districtIdFk;
    }

    public void setDistrictIdFk(Integer districtIdFk) {
        this.districtIdFk = districtIdFk;
    }

    public Object getTehsilIdFk() {
        return tehsilIdFk;
    }

    public void setTehsilIdFk(Object tehsilIdFk) {
        this.tehsilIdFk = tehsilIdFk;
    }

    public Object getCenterIdFk() {
        return centerIdFk;
    }

    public void setCenterIdFk(Object centerIdFk) {
        this.centerIdFk = centerIdFk;
    }

    public Object getMauzaIdFk() {
        return mauzaIdFk;
    }

    public void setMauzaIdFk(Object mauzaIdFk) {
        this.mauzaIdFk = mauzaIdFk;
    }

    public Object getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getAppUserToken() {
        return appUserToken;
    }

    public void setAppUserToken(String appUserToken) {
        this.appUserToken = appUserToken;
    }

    public Object getAppUserTokenExpiry() {
        return appUserTokenExpiry;
    }

    public void setAppUserTokenExpiry(Object appUserTokenExpiry) {
        this.appUserTokenExpiry = appUserTokenExpiry;
    }

    public Object getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Object profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
