package pk.gov.pitb.sugarcanetpv;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.multidex.MultiDex;

import com.orm.SugarContext;

import java.io.File;


public class SugarTrackingApplication extends Application {

    public static String TAG = "SugarTrakingApplication";
    private static SugarTrackingApplication instance;
    public static Context context;
    private static File cacheDir;
    private Activity activeActivity;
    public static MediaPlayer mediaPlayer;
    public static Boolean streamState = Boolean.valueOf(true);


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
//        MultiDex.install(this);
        instance = this;
        SugarTrackingApplication.context = getApplicationContext();
        setupActivityListener();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        SugarContext.init(base);
        MultiDex.install(base);
    }

    public SugarTrackingApplication() {
    }

    public static SugarTrackingApplication getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return instance;
    }

    public static Resources getAppResources() {
        return instance.getResources();
    }

    public static Application getApplicationInstance() {
        return instance;
    }

    private void setupActivityListener() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            }

            @Override
            public void onActivityStarted(Activity activity) {
            }

            @Override
            public void onActivityResumed(Activity activity) {
                activeActivity = activity;
            }

            @Override
            public void onActivityPaused(Activity activity) {
//                final Intent serviceIntent = new Intent(DFDApplication.context, SpentTimeService.class);
//                startService(serviceIntent);
                activeActivity = null;
            }

            @Override
            public void onActivityStopped(Activity activity) {
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
            }
        });
    }

    public Activity getActiveActivity() {
        return activeActivity;
    }
}