package pk.gov.pitb.sugarcanetpv.helperClasses;

/**
 * Created by pitb on 3/30/2016.
 */
public class Constants {


    public static String BASE_URL = "";


    /* DATE FORMAT */
    public static final String FORMAT_TIME_APP = "HH:mm:ss";
//    public static final String FORMAT_DATE_APP = "yyyy-MM-dd";
    public static final String FORMAT_DATE_APP = "dd-MM-yyyy";
//    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_TIME = "dd-MM-yyyy HH:mm:ss";
//    public static final String VIEW_FORMAT_DATE_TIME = "dd-MM-yyyy hh:mm a";
//    public static final String VIEW_FORMAT_DATE_TIME = "dd-MM-yyyy hh:mm a"; //2021-01-13 19:25:59
//    public static final String VIEW_FORMAT_DATE_TIME_PAYMENT = "dd-MM-yyyy";
    public static final String SERVER_FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    /*    */



}
