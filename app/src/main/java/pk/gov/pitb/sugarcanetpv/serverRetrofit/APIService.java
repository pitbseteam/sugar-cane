package pk.gov.pitb.sugarcanetpv.serverRetrofit;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pk.gov.pitb.sugarcanetpv.models.PaymentResponse;
import pk.gov.pitb.sugarcanetpv.models.SaveShipmentResponse;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.LoginResponse;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SyncResponse;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by PITB on 10/18/2017.
 */

public interface APIService {

    @POST("sugarcaneSync")
    @FormUrlEncoded
    Call<SyncResponse> syncData(@Header("hashkey") String hashKey, @FieldMap Map<String, String> data);

    @POST("sugarcaneLogin")
    @FormUrlEncoded
    Call<LoginResponse> login(@Header("hashkey") String hashKey, @FieldMap Map<String, String> data);


    @Multipart
    @POST("saveTPVData")
    Call<SaveShipmentResponse> saveShipment(@Header("hashkey") String hashKey, @Part("data") RequestBody data,
                                        @Part("app_data") RequestBody appData);



}
