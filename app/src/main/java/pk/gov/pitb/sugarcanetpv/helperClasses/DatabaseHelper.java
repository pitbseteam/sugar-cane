package pk.gov.pitb.sugarcanetpv.helperClasses;

import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.Shipment;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SugarMill;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.TpvDatum;

public class DatabaseHelper {

    public static void resetDatabaseSyncData(){
        try{
            SugarMill.deleteAll(SugarMill.class);
            TpvDatum.deleteAll(TpvDatum.class);
            Shipment.deleteAll(Shipment.class, "isunsent = ?", "false");

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void resetAllDatabase(){
        try{
            UserData.deleteAll(UserData.class);
            Shipment.deleteAll(Shipment.class);
            resetDatabaseSyncData();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
