package pk.gov.pitb.sugarcanetpv.Ui.userSigningActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.gson.JsonObject;
import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.SplashScreen;
import pk.gov.pitb.sugarcanetpv.dialogues.SweetAlertDialogs;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.LoginResponse;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pk.gov.pitb.sugarcanetpv.SplashScreen.siteKey;

public class SignInActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {

    @BindView(R.id.txtLogin_cnic)
    EditText username;
    @BindView(R.id.txtLogin_password)
    EditText password;
    @BindView(R.id.login)
    Button login;

    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;
    String recaptchaToken = "";
    private GoogleApiClient googleApiClient;

    private void setInstances(){
        mGlobals = Globals.getInstance(this);
        SugarContext.init(this);
        ButterKnife.bind(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);
    }

    @Override
    protected void onResume() {
        setInstances();
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_screen);

        setInstances();
        listeners();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void listeners() {
//        username.addTextChangedListener(new TextWatcherCNIC(username));
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(SafetyNet.API)
                .addConnectionCallbacks(SignInActivity.this)
                .build();
        googleApiClient.connect();

        username.setText("sugarcane_tpv_user_ryk");
        password.setText("p@kistan!23");

        login.setOnClickListener(v -> {
            if (validateFields()) {
                login();
            }
        });
    }

    private boolean validateFields() {
        if(username.getText().toString().equals("")){
            username.setError(this.getResources().getString(R.string.enter_user));
            username.requestFocus();
            return false;
        }
        if(password.getText().toString().equals("")){
            password.setError(this.getResources().getString(R.string.enter_pass));
            password.requestFocus();
            return false;
        }
        if(password.getText().toString().length()<8){
            password.setError(this.getResources().getString(R.string.enter_correct_pass));
            password.requestFocus();
            return false;
        }
        return true;
    }


    public void forgotPassword(View view){
        //Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
        Intent intent = new Intent(SignInActivity.this, NewPasswordActivity.class);
        startActivity(intent);
    }


    private void login(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(Globals.getUsage().mContext.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("username", username.getText().toString());
            jsonObject.addProperty("password", password.getText().toString());
//            jsonObject.addProperty("token", recaptchaToken);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());

        mAPIService.login(siteKey(),hashMap).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Log.i("TAG", "post submitted to API." + response.body().toString());
                    LoginResponse loginResponse = response.body();
                    if(loginResponse.getStatus()){
                        // login success
                        Globals.getUsage().sharedPreferencesEditor.setLoggedIn(true);
                        UserData userData = loginResponse.getUserData();
                        if(userData!=null) {
                            userData.save();
                            openSplashActivity();
                        }else{
                            SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Error", "User Data is not there in response", null, false);
                        }
                    }else{
                        SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, getString(R.string.cannot_signin), loginResponse.getMessage(), null, false);
                    }
                } else {
                    Globals.getUsage().showDialogFailure("Please contact admin");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("failure", "Unable to submit post to API.");
                if (t instanceof IOException) {
                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.network_error));     // logging probably not necessary
                }
                else {
                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.error_message));
                }
            }
        });
    }


    public void openSplashActivity(){
        Globals.getUsage().sharedPreferencesEditor.setToBeVerified(true);
        Intent intent = new Intent(SignInActivity.this, SplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
