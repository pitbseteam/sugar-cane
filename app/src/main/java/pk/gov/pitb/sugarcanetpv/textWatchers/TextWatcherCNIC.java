package pk.gov.pitb.sugarcanetpv.textWatchers;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherCNIC implements TextWatcher {


	private EditText editText = null;

	public TextWatcherCNIC(EditText editText) {
		this.editText = editText;
//		this.editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		InputFilter inputFilterNumber = (source, start, end, dest, dstart, dend) -> {
			for (int i = start; i < end; i++) {
				if (!(Character.isDigit(source.charAt(i)) || source.charAt(i) == '-')) {
					return "";
				}
			}
			return null;
		};
		this.editText.setFilters(new InputFilter[]{inputFilterNumber, new InputFilter.LengthFilter(15)});
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		try {
			String textFormatted = editText.getText().toString().trim();
			String textOriginal = textFormatted.replaceAll("-", "").trim();
			if (textOriginal.length() >= 13 && (textFormatted.length() <= textOriginal.length() || textFormatted.charAt(13) != '-')) {
				editText.setText(textOriginal.substring(0, 5) + "-" + textOriginal.substring(5, 12) + "-" + textOriginal.substring(12));
				editText.setSelection(editText.getText().toString().length());
			} else if (textOriginal.length() >= 6 && textFormatted.charAt(5) != '-') {
				editText.setText(textOriginal.substring(0, 5) + "-" + textOriginal.substring(5));
				editText.setSelection(editText.getText().toString().length());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void afterTextChanged(Editable s) {
	}
}