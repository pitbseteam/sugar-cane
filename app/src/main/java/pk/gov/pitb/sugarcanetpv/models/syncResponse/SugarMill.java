
package pk.gov.pitb.sugarcanetpv.models.syncResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class SugarMill  extends SugarRecord implements Serializable {

    @SerializedName("sm_id")
    @Expose
    private Integer smId;
    @SerializedName("sm_name")
    @Expose
    private String smName;
    @SerializedName("district_idFk")
    @Expose
    private Integer districtIdFk;
    @SerializedName("division_idFk")
    @Expose
    private Integer divisionIdFk;
    @SerializedName("sm_status")
    @Expose
    private Integer smStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("updated_by")
    @Expose
    private Integer updatedBy;
    @SerializedName("district_name")
    @Expose
    private String districtName;

    public Integer getSmId() {
        return smId;
    }

    public void setSmId(Integer smId) {
        this.smId = smId;
    }

    public String getSmName() {
        return smName;
    }

    public void setSmName(String smName) {
        this.smName = smName;
    }

    public Integer getDistrictIdFk() {
        return districtIdFk;
    }

    public void setDistrictIdFk(Integer districtIdFk) {
        this.districtIdFk = districtIdFk;
    }

    public Integer getDivisionIdFk() {
        return divisionIdFk;
    }

    public void setDivisionIdFk(Integer divisionIdFk) {
        this.divisionIdFk = divisionIdFk;
    }

    public Integer getSmStatus() {
        return smStatus;
    }

    public void setSmStatus(Integer smStatus) {
        this.smStatus = smStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

}
