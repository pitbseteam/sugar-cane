package pk.gov.pitb.sugarcanetpv.Ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.SharedPreferences.PreferenceData;
import pk.gov.pitb.sugarcanetpv.Ui.userSigningActivities.SignInActivity;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;

public class MainScreenActivity extends AppCompatActivity {

    @BindView(R.id.button)
    Button btn;

    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;

    private void setInstances(){
        mGlobals = Globals.getInstance(this);
        SugarContext.init(this);
        ButterKnife.bind(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        setInstances();
        initializeViews();
    }

    private void initializeViews() {
        if(PreferenceData.getUserLoggedInStatus(this)) {
            btn.setText("Continue as " + PreferenceData.getLoggedInCnic(this));
            btn.setBackgroundColor(Color.rgb(44, 159, 252));
            btn.setTextColor(Color.WHITE);
        }
        /*else{
            btn.setText("Continue as " + PreferenceData.getLoggedInCnic(this));
            btn.setBackgroundColor(Color.BLUE);
            btn.setTextColor(Color.WHITE);
        }*/
    }

    public void toLoginScreen(View view){
        Intent intent = new Intent(MainScreenActivity.this, SignInActivity.class);
        startActivity(intent);
    }


}
