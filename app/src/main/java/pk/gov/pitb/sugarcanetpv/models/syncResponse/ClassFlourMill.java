
package pk.gov.pitb.sugarcanetpv.models.syncResponse;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ClassFlourMill extends SugarRecord implements Serializable {

    @SerializedName("name")
    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
