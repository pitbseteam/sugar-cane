package pk.gov.pitb.sugarcanetpv;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jaeger.library.StatusBarUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orm.SugarContext;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pk.gov.pitb.sugarcanetpv.Ui.DrawerMainActivity;
import pk.gov.pitb.sugarcanetpv.Ui.userSigningActivities.SignInActivity;
import pk.gov.pitb.sugarcanetpv.asynctasks.AsyncTaskSaveSyncedData;
import pk.gov.pitb.sugarcanetpv.dialogues.SweetAlertDialogs;
import pk.gov.pitb.sugarcanetpv.handler.HandlerDataSync;
import pk.gov.pitb.sugarcanetpv.helperClasses.Constants;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.SaveShipmentResponse;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.Shipment;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SyncResponse;
import pk.gov.pitb.sugarcanetpv.requestPermisson.RequestPermissionsHelper;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashScreen extends AppCompatActivity {

    private static final String TAG = "syncResponse";
    public static final int PERMISSION_REQUEST_CODE = 0;
    private APIService mAPIService;
    Thread thread=null;
    SweetAlertDialog progressDialog;
    boolean checkInResume = false;

    @BindView(R.id.tv_app_version)
    TextView textViewAppVersion;


    // Used to load the 'native-lib' library onoldAPIsURL application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkInResume) {
            checkInResume = false;
            requestPermission();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        requestPermission();
//    setToWait();
    }
    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            int permission = RequestPermissionsHelper.requestPermission(SplashScreen.this);
            if(permission == RequestPermissionsHelper.requestPermissionCode){
                ActivityCompat.requestPermissions(SplashScreen.this, RequestPermissionsHelper.persmissionsStringArray, SplashScreen.PERMISSION_REQUEST_CODE);
            }else if(permission == RequestPermissionsHelper.notShouldShowRequestPermissionRationaleCode){
                showDialogPermissionDenied();
            } if(permission == RequestPermissionsHelper.notCheckSelfPermissionCode) {
                initializeApplication();
            }
        } else {
            initializeApplication();
        }
    }
    public void initializeApplication() {
        Globals.getInstance(this);
        SugarContext.init(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);
        ButterKnife.bind(this);

        textViewAppVersion.setText("version " + Globals.getAppVersionName());

        Constants.BASE_URL = baseURL();

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
        if(Globals.getUsage().sharedPreferencesEditor.IsLoggedIn()){
            if(Globals.getUsage().isInternetAvailable()) {
                if (Globals.getUsage().sharedPreferencesEditor.getSyncRequired()) {
                    syncData();
                }else if(Shipment.count(Shipment.class, "isunsent = ?", new String[]{"true"})>0){
                    sendUnsent();
                }else{
                    setToWait();
                }
            }else {
                if(Globals.getUsage().sharedPreferencesEditor.getSyncRequired()) {
                    SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "No Internet", "Please connect to internet",
                            new SweetAlertDialogs.OnDialogClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            }, false);
                }else {
                    setToWait();
                }
            }
        }else{
            startLoginActivity();
        }
    }
    public void setToWait(){
        thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2000);

                    startMainActivity();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case SplashScreen.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0){
                    boolean granted = true;
                    for(int i=0;i<grantResults.length;i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            granted=false;
                            break;
                        }
                    }
                    if(granted){
                        initializeApplication();
                    }else{
                        showDialogPermissionDenied();
                    }
                } else {
                    showDialogPermissionDenied();
                }
                break;
        }
    }
    public void startMainActivity(){
        Intent intent = new Intent(SplashScreen.this, DrawerMainActivity.class);
        startActivity(intent);
        finish();
    }
    public void startLoginActivity(){
        Intent intent = new Intent(SplashScreen.this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    public void showDialogPermissionDenied(){

        SweetAlertDialogs.getInstance().showDialogPermissionDenied(this, new SweetAlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                checkInResume = true;
                sweetAlertDialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
//                finish();
            }
        });

    }


    public void syncData() {
        progressDialog = new SweetAlertDialog(Globals.getUsage().mContext, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setTitle("Syncing");
        progressDialog.setContentText("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        UserData userData = new UserData();

        if(UserData.count(UserData.class)>0)
            userData = UserData.listAll(UserData.class).get(0);

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", userData.getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());

        mAPIService.syncData(siteKey(), hashMap).enqueue(new Callback<SyncResponse>() {
            @Override
            public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {

                if (response.isSuccessful()) {
                    Log.i(TAG, "Synced" + response.body().toString());
                    SyncResponse syncResponse = response.body();
                    if(syncResponse.getStatus()){
                        try {
                            AsyncTaskSaveSyncedData asyncTaskSaveSyncedData = new AsyncTaskSaveSyncedData(handlerSync, syncResponse);
                            asyncTaskSaveSyncedData.execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        progressDialog.dismiss();
                        SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext,
                                "Server Error - سرور کی خرابی", syncResponse.getMessage(),new SweetAlertDialogs.OnDialogClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        finish();
                                    }
                                }, false);

                    }
                }else if (response.errorBody() != null) {
                    // Get response errorBody
                    progressDialog.dismiss();
                    try {
                        String errorBody = response.errorBody().string();
                        Log.i(TAG, "error#####  " + response.errorBody().toString());
                        SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext,
                                "Server Error - سرور کی خرابی", "Please contact admin - ایڈمن سے رابطہ کریں", new SweetAlertDialogs.OnDialogClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        finish();
                                    }
                                }, false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    progressDialog.dismiss();
                    SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, getString(R.string.error), "Please contact admin - ایڈمن سے رابطہ کریں", new SweetAlertDialogs.OnDialogClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    }, false);
                }
            }
            @Override
            public void onFailure(Call<SyncResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, "Unable to sync - مطابقت پذیری سے قاصر");
                if (t instanceof IOException) {
                    SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Unable to sync - مطابقت پذیری سے قاصر",
                            getString(R.string.network_error), new SweetAlertDialogs.OnDialogClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            }, false);
                }
                else {
                    SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Unable to sync - مطابقت پذیری سے قاصر", "Please try again later - دوبارہ کوشش کریں", new SweetAlertDialogs.OnDialogClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    }, false);

                }

            }
        });
    }

    private HandlerDataSync handlerSync = new HandlerDataSync() {

        @Override
        public void onSyncSuccessful(boolean success) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if(success) {
                Globals.getUsage().sharedPreferencesEditor.setSyncRequired(false);
                if(Shipment.count(Shipment.class, "isunsent = ?", new String[]{"true"})>0){
                    sendUnsent();
                }else{
                    setToWait();
                }
            }
        }
    };



    int unsentTreverser = 0;
    List<Shipment> ClassFarmerList;
    public void sendUnsent() {

        ClassFarmerList = Shipment.find(Shipment.class, "isunsent = ?", "true");

        progressDialog = new SweetAlertDialog(Globals.getUsage().mContext, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setContentText("Please wait...");
        progressDialog.setTitle("Sending Unsent Data");
        progressDialog.setCancelable(false);
        progressDialog.show();

        submit();
    }

    private void submit() {

        final Shipment classShipmentsForm = ClassFarmerList.get(unsentTreverser);


        Gson gson = new Gson();
        String jsonString = gson.toJson(classShipmentsForm);
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(jsonString);
        JsonObject jsonObject = json.getAsJsonObject();


        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());

        RequestBody requestBodyData = RequestBody.create(MediaType.parse("text/plain"),jsonObject.toString());
        RequestBody requestBodyAppData = RequestBody.create(MediaType.parse("text/plain"),Globals.getUsage().getJSON().toString());

        MultipartBody.Part bodyImageFile;

            mAPIService.saveShipment(siteKey(), requestBodyData,requestBodyAppData)
                    .enqueue(new Callback<SaveShipmentResponse>() {
                        //        mAPIService.saveFarmer(hashMap).enqueue(new Callback<GeneralResponse>() {
                        @Override
                        public void onResponse(Call<SaveShipmentResponse> call, Response<SaveShipmentResponse> response) {
                            onResponseCase(classShipmentsForm, response);
                        }

                        @Override
                        public void onFailure(Call<SaveShipmentResponse> call, Throwable t) {

                            onFailureCase(t);
                        }
                    });


    }

    public void onResponseCase(Shipment classShipmentsForm, Response<SaveShipmentResponse> response){
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            Log.i("TAG", "post submitted to API." + response.body().toString());

            SaveShipmentResponse saveResponse = response.body();

            if (saveResponse.getStatus()) {

                // success
                Shipment shipment = response.body().getData();
                  //update object in db from db
                classShipmentsForm.delete();
                classShipmentsForm.setIsunsent("false");
                classShipmentsForm.save();

                unsentTreverser++;
                if (unsentTreverser < ClassFarmerList.size()) {
                    submit();
                }else {
                    progressDialog.dismiss();
                    setToWait();
                }
            } else {
                if(saveResponse.getCode()==420){
                    classShipmentsForm.delete();
                    unsentTreverser++;
                    if (unsentTreverser < ClassFarmerList.size()) {
                        submit();
                    }else {
                        progressDialog.dismiss();
                        setToWait();
                    }
                }else {
                    showDialogTryLater();
                }
            }
        } else {
            showDialogTryLater();
        }

    }


    public void onFailureCase(Throwable t){
        progressDialog.dismiss();
        Log.e("failure", "Unable to submit post to API.");
        if (t instanceof IOException) {
            SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Network Error",
                    Globals.getUsage().mContext.getString(R.string.try_later), new SweetAlertDialogs.OnDialogClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            setToWait();
                        }
                    }, false);
        } else {
            showDialogTryLater();
//                            SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, getString(R.string.error_send_unsent), getString(R.string.error_message), sweetAlertDialog -> {sweetAlertDialog.dismiss();}, false);
        }
    }
    public void showDialogTryLater(){
        SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, getString(R.string.error_send_unsent),
                Globals.getUsage().mContext.getString(R.string.try_later), new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        setToWait();
                    }
                }, false);
    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public static native String baseURL();
    public static native String siteKey();

}
