package pk.gov.pitb.sugarcanetpv.textWatchers;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherPhone implements TextWatcher {

    private EditText editText = null;

    public TextWatcherPhone(EditText editText) {
        this.editText = editText;
        InputFilter inputFilterNumber = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                if (!(Character.isDigit(source.charAt(i)) || source.charAt(i) == '-')) {
                    return "";
                }
            }
            return null;
        };
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            String textFormatted = editText.getText().toString().trim();
            String textOriginal = s.toString();
            if (textOriginal.length() == 1 && !textOriginal.equals("0")) {
                editText.setText("");
            } else if (textOriginal.length() > 1 && !textOriginal.startsWith("0")) {
                int length = editText.getText().toString().length();
                editText.setText(textOriginal.substring(1, length));
                editText.setSelection(length);
            } else if (textOriginal.length() == 5 && !textOriginal.endsWith("-")) {
                editText.setText(textOriginal.substring(0, 4) + "-" + textOriginal.substring(4));
                editText.setSelection(editText.getText().toString().length());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void afterTextChanged(Editable s) {
    }
}