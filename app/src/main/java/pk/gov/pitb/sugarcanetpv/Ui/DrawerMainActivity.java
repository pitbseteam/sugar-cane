package pk.gov.pitb.sugarcanetpv.Ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.orm.SugarContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.SplashScreen;
import pk.gov.pitb.sugarcanetpv.Ui.Fragments.ShipmentListFragment;
import pk.gov.pitb.sugarcanetpv.Ui.Fragments.UnsentFragment;
import pk.gov.pitb.sugarcanetpv.Ui.userSigningActivities.NewPasswordActivity;
import pk.gov.pitb.sugarcanetpv.dialogues.SweetAlertDialogs;
import pk.gov.pitb.sugarcanetpv.helperClasses.DatabaseHelper;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.Shipment;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;

public class DrawerMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.tv_title)
    public AppCompatTextView mTextView;

    NavigationView navigationView;
    ActionBarDrawerToggle mDrawerToggle;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    public static APIService mAPIService;
    public ProgressDialog progressDialog;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private FragmentTransaction fragmentTransaction;

    ShipmentListFragment shipmentListFragment;
    UnsentFragment unsentFragment;
    private DisplayImageOptions options;
    UserData userData;

    private  void setInstances(){
        Globals.getInstance(this);
        ButterKnife.bind(this);
        SugarContext.init(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);
    }

    @Override
    protected void onResume() {
        setInstances();
        setUserInfo();
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_main);
        setInstances();
        initializeActivity();
    }

    public void initializeActivity(){
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.logo)
                .showImageForEmptyUri(R.drawable.logo)
                .showImageOnFail(R.drawable.logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setUserInfo();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTextView.setTransitionName(getString(R.string.app_name));
            mTextView.setText(getString(R.string.app_name));
        }

        mDrawerToggle  = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

            openSaveShipmentsFragment();
    }

    @SuppressLint("SetTextI18n")
    public void setUserInfo(){
        userData = UserData.first(UserData.class);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername =  headerView.findViewById(R.id.textview_header);


        if(userData.getRoleName()!= null){
            // if center app then use user data
            navUsername.setText(userData.getName());
        }
    }
    public void openSaveShipmentsFragment(){
        fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(shipmentListFragment ==null)
            shipmentListFragment = new ShipmentListFragment();
        fragmentTransaction.replace(R.id.layout_fragment, shipmentListFragment, "Sugar MillShi List");
        fragmentTransaction.addToBackStack(shipmentListFragment.getClass().getName());
        fragmentTransaction.commit();

    }


    private void openUnsentFragment() {
        if(Shipment.count(Shipment.class, "isunsent = ?", new String[]{"true"})>0) {
            mTextView.setText("Unsent");
            if (unsentFragment == null)
                unsentFragment = new UnsentFragment();
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.layout_fragment, unsentFragment, "Unsent");
            fragmentTransaction.addToBackStack(unsentFragment.getClass().getName());
            fragmentTransaction.commit();
        }else{
            Toast.makeText(this, "No unsent available", Toast.LENGTH_SHORT).show();
        }
    }
    public void openSaveShipmentsListingActivity(){

        Intent intent = new Intent(DrawerMainActivity.this, ShipmentFormActivity.class);
        startActivity(intent);



//        fragmentManager = getSupportFragmentManager();
//        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        if(farmerRegisterFragment==null)
//            farmerRegisterFragment = new FarmerRegisterFragment();
//        fragmentTransaction.replace(R.id.layout_fragment, farmerRegisterFragment, "Farmers Register");
//        fragmentTransaction.addToBackStack(farmerRegisterFragment.getClass().getName());
//        fragmentTransaction.commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (fragmentManager.getBackStackEntryCount() - 1 > 0) {
//            if(fragmentManager.getBackStackEntryCount() - 1==1){
//                //dashboard fragment
//                showBackButton(true);
//            }
            fragmentManager.popBackStack();
        }
        else{
            Fragment currentFragment = fragmentManager.findFragmentById(R.id.layout_fragment);
            if (currentFragment instanceof ShipmentListFragment) {
                alertViewExit();
            }
//            else if (currentFragment instanceof PaymentVerificationFragment){
//                alertViewExit();
//            }else{
//                openSaveShipmentsFragment();
//
//            }
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.layout_fragment);

        if (id == R.id.nav_sync) {
            syncApp();
        } else if (id == R.id.nav_logout) {
            logout();
        }else if (id == R.id.nav_unsent) {
            if (!(currentFragment instanceof UnsentFragment))
                openUnsentFragment();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout(){

        SweetAlertDialogs.getInstance().showDialogYesNo(this, "Logout - لاگ آوٹ", "Are you sure you want to logout?\nکیا آپ واقعی لاگ آؤٹ کرنا چاہتے ہیں؟", new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        if(Shipment.count(Shipment.class, "isunsent = ?", new String[]{"true"})>0){
                            Toast.makeText(Globals.getUsage().mContext, "Submit unsent data first", Toast.LENGTH_SHORT).show();
                            openUnsentFragment();
                        }else {
                            Globals.getUsage().sharedPreferencesEditor.clearPreferences();
                            DatabaseHelper.resetAllDatabase();
                            Intent intent = new Intent(DrawerMainActivity.this, SplashScreen.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                },
                new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                }, false);
    }

    public void changePassword(){
        Intent intent = new Intent(this, NewPasswordActivity.class);
        startActivity(intent);
    }

    public void syncApp(){
        if(Globals.getUsage().isInternetAvailable()) {
            Globals.getUsage().sharedPreferencesEditor.setSyncRequired(true);
            DatabaseHelper.resetDatabaseSyncData();
            Intent intent = new Intent(this, SplashScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, "Internet not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void showBackButton(boolean enable) {

        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if(enable) {
            //You may not want to open the drawer on swipe from the left in this case
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            // Remove hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            // Show back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if(!mToolBarNavigationListenerIsRegistered) {
                mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed
                        onBackPressed();
                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            //You must regain the power of swipe for the drawer.
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            // Remove back button
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            mDrawerToggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }

        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.
    }

    private void alertViewExit() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Exit? | باہر نکلیں")
                .setMessage(Globals.getUsage().mContext.getResources().getString(R.string.exit_message))
                .setPositiveButton("Yes - جی ہاں", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("No - نہیں", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                    }
                })
                .show();
    }



    public void setmTextViewText(String text){
        mTextView.setText(text);

    }
}
