
package pk.gov.pitb.sugarcanetpv.models.syncResponse;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class SyncData {

    @SerializedName("sugar_mills")
    @Expose
    private List<SugarMill> sugarMills = null;
//    @SerializedName("tpv_data")
//    @Expose
//    private List<TpvDatum> tpvData = null;

    public List<SugarMill> getSugarMills() {
        return sugarMills;
    }

    public void setSugarMills(List<SugarMill> sugarMills) {
        this.sugarMills = sugarMills;
    }

//    public List<TpvDatum> getTpvData() {
//        return tpvData;
//    }
//
//    public void setTpvData(List<TpvDatum> tpvData) {
//        this.tpvData = tpvData;
//    }
}
