package pk.gov.pitb.sugarcanetpv.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.Ui.ShipmentFormActivity;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.loginResponse.UserData;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SugarMill;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SugarMill;


public class FlourMillListAdapter extends RecyclerView.Adapter<FlourMillListAdapter.ViewHolder> {

    List<SugarMill> mList;
    Context context;
    int index;
    UserData userData;

    public FlourMillListAdapter(List<SugarMill> mList, Context context) {

        setHasStableIds(true);
        this.mList = mList;
        this.context = context;
        userData = UserData.first(UserData.class);
    }

    @Override
    public FlourMillListAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {

        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_items, viewGroup, false);

        final ViewHolder holder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = holder.getAdapterPosition();
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(FlourMillListAdapter.ViewHolder viewHolder, int position) {
        index = position;
        viewHolder.textViewOrderNo.setText("Sugar Mill Name: " +mList.get(position).getSmName());
        viewHolder.textViewSSPort.setText("District Name: " + mList.get(position).getDistrictName() );

        viewHolder.linearLayoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Globals.getUsage().mContext, ShipmentFormActivity.class);
                intent.putExtra("sugarmill",mList.get(position));
                Globals.getUsage().mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout linearLayoutMain;
        public TextView textViewOrderNo, textViewRegDate, textViewSSPort, textViewSSRate, textViewAmount;
        public ImageView imageView;
//        public ProgressWheel progressWheel;

        public ViewHolder( View itemView) {
            super(itemView);

            linearLayoutMain = itemView.findViewById(R.id.ll_main);
            textViewOrderNo = itemView.findViewById(R.id.tv_order_no);
            textViewSSRate = itemView.findViewById(R.id.tv_ss_rate);
            textViewSSPort = itemView.findViewById(R.id.tv_ss_port);
            textViewAmount = itemView.findViewById(R.id.tv_amount);
            textViewRegDate = itemView.findViewById(R.id.tv_reg_date);
            imageView = itemView.findViewById(R.id.imageView);
//            progressWheel = itemView.findViewById(R.id.image_progress_wheel);
        }
    }

    public void updateList(List<SugarMill> newlist) {
        this.mList.clear();
        this.mList.addAll(newlist);
        this.notifyDataSetChanged();
    }

}
