package pk.gov.pitb.sugarcanetpv.asynctasks;

import android.app.AlertDialog;
import android.os.AsyncTask;

import org.json.JSONObject;

import pk.gov.pitb.sugarcanetpv.handler.HandlerDataSync;
import pk.gov.pitb.sugarcanetpv.helperClasses.DatabaseHelper;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SugarMill;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SyncResponse;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.TpvDatum;


public class AsyncTaskSaveSyncedData extends AsyncTask<Void, Void, Void> {

    private AlertDialog progressDialog;
    private HandlerDataSync handlerDataSync;
    private SyncResponse syncResponse;
    boolean syncSuccess = false;
    JSONObject response;

    public AsyncTaskSaveSyncedData(HandlerDataSync handlerDataSync, SyncResponse syncResponse) {
        super();
        this.response = response;
        this.handlerDataSync = handlerDataSync;
        this.syncResponse = syncResponse;
    }

    public AsyncTaskSaveSyncedData(HandlerDataSync handlerDataSync) {
        super();
        this.response = response;
        this.handlerDataSync = handlerDataSync;
    }

    public void execute() throws Exception {
        execute((Void) null);
    }

    @Override
    protected void onPreExecute() {
        try {
//			progressDialog = new ProgressDialog(Globals.getUsage().mContext);
//			progressDialog.setTitle("Saving data...");
//			progressDialog.setMessage("Please wait...");
//			progressDialog.setCancelable(false);
//			progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            //save districts
            DatabaseHelper.resetDatabaseSyncData();
            SugarMill.saveInTx(syncResponse.getData().getSugarMills());
//            TpvDatum.saveInTx(syncResponse.getData().getTpvData());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        try {
            syncSuccess = true;
            handlerDataSync.onSyncSuccessful(syncSuccess);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}