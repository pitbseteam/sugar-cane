package pk.gov.pitb.sugarcanetpv.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import butterknife.ButterKnife;
import pk.gov.pitb.sugarcanetpv.Adapters.unsentListAdapter;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.SplashScreen;
import pk.gov.pitb.sugarcanetpv.Ui.DrawerMainActivity;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.Shipment;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;

public class UnsentFragment extends Fragment {

    RecyclerView recyclerView;
    Button buttonSearchFarmer;
    unsentListAdapter farmerListAdapter;
    List<Shipment> ClassFarmerList;
    private View parentView;
    private DrawerMainActivity activity;
    private APIService mAPIService;
    Globals mGlobals;

    public UnsentFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {

        if (farmerListAdapter != null) {
            ClassFarmerList = Shipment.find(Shipment.class, "isunsent = ?", "true");
            farmerListAdapter.updateList(ClassFarmerList);
        }

        activity.setmTextViewText("Unsent List");

        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(parentView == null){
            parentView = inflater.inflate(R.layout.fragment_unsent_farmer_list, container, false);
        }
        ButterKnife.bind(this, parentView);

        ClassFarmerList = Shipment.find(Shipment.class, "isunsent = ?", "true");

        return parentView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setInstances();
        generateBody();
    }

    private  void setInstances(){
        mGlobals = Globals.getInstance(getActivity());
        mAPIService = ApiUtils.getAPIService();
    }

    public void generateBody(){

        activity = (DrawerMainActivity) getActivity();

        setAdapter();
        buttonSearchFarmer = getActivity().findViewById(R.id.button_search_farmer);

        buttonSearchFarmer.setText("Send All");

        buttonSearchFarmer.setOnClickListener(v -> {

            //TODO SEND UNSENT

            if(ClassFarmerList !=null && ClassFarmerList.size()>0){
                if(mGlobals.isInternetAvailable())
                    openSplashActivity();
                else
                    Toast.makeText(mGlobals.mContext, "Internet not available", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(mGlobals.mContext, "non unsent available", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(){
        recyclerView = activity.findViewById(R.id.recycler_view_videos);

        RecyclerView.LayoutManager lm1 = new GridLayoutManager(getActivity(), 1);
        ((GridLayoutManager) lm1).setOrientation(GridLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(lm1);
        recyclerView.setHasFixedSize(true);

        farmerListAdapter = new unsentListAdapter(ClassFarmerList, getActivity());
        recyclerView.setAdapter(farmerListAdapter);
    }

    public void openSplashActivity(){
        Intent intent = new Intent(activity, SplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

}
