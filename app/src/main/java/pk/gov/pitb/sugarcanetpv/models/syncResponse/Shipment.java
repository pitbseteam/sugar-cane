
package pk.gov.pitb.sugarcanetpv.models.syncResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class Shipment extends SugarRecord implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("mill_idFk")
    @Expose
    private Integer millIdFk;
    @SerializedName("mill_name")
    @Expose
    private String millName;
    @SerializedName("std_purchased")
    @Expose
    private Float stdPurchased;
    @SerializedName("std_crushed")
    @Expose
    private Float stdCrushed;
    @SerializedName("std_sugar_poduced")
    @Expose
    private Float stdSugarProduced;
    @SerializedName("std_sugar_sold")
    @Expose
    private Float stdSugarSold;
    @SerializedName("std_sugar_not_lifted")
    @Expose
    private Float stdSugarNotLifted;
    @SerializedName("std_sugar_stock")
    @Expose
    private Float stdSugarStocked;

    @SerializedName("std_activity_datetime")
    @Expose
    private String mSsActivityDatetime;

    @SerializedName("std_lat_long")
    @Expose
    private String mSsLatLong;

    @SerializedName("is_unsent")
    @Expose
    private String isunsent = "false";


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getMillIdFk() {
        return millIdFk;
    }

    public void setMillIdFk(Integer millIdFk) {
        this.millIdFk = millIdFk;
    }

    public Float getStdPurchased() {
        return stdPurchased;
    }

    public void setStdPurchased(Float stdPurchased) {
        this.stdPurchased = stdPurchased;
    }

    public Float getStdCrushed() {
        return stdCrushed;
    }

    public void setStdCrushed(Float stdCrushed) {
        this.stdCrushed = stdCrushed;
    }

    public Float getStdSugarProduced() {
        return stdSugarProduced;
    }

    public void setStdSugarProduced(Float stdSugarProduced) {
        this.stdSugarProduced = stdSugarProduced;
    }

    public Float getStdSugarSold() {
        return stdSugarSold;
    }

    public void setStdSugarSold(Float stdSugarSold) {
        this.stdSugarSold = stdSugarSold;
    }

    public Float getStdSugarNotLifted() {
        return stdSugarNotLifted;
    }

    public void setStdSugarNotLifted(Float stdSugarNotLifted) {
        this.stdSugarNotLifted = stdSugarNotLifted;
    }

    public Float getStdSugarStocked() {
        return stdSugarStocked;
    }

    public void setStdSugarStocked(Float stdSugarStocked) {
        this.stdSugarStocked = stdSugarStocked;
    }

    public String getmSsActivityDatetime() {
        return mSsActivityDatetime;
    }

    public void setmSsActivityDatetime(String mSsActivityDatetime) {
        this.mSsActivityDatetime = mSsActivityDatetime;
    }

    public String getmSsLatLong() {
        return mSsLatLong;
    }

    public void setmSsLatLong(String mSsLatLong) {
        this.mSsLatLong = mSsLatLong;
    }

    public String getIsunsent() {
        return isunsent;
    }

    public void setIsunsent(String isunsent) {
        this.isunsent = isunsent;
    }

    public String getMillName() {
        return millName;
    }

    public void setMillName(String millName) {
        this.millName = millName;
    }
}
