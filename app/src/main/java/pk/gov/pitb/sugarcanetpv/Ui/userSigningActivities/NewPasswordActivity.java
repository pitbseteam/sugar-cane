package pk.gov.pitb.sugarcanetpv.Ui.userSigningActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;

import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;

public class NewPasswordActivity extends AppCompatActivity {

    @BindView(R.id.txtNewPassword_password)
    EditText pass;
    @BindView(R.id.txtNewPassword_confirmPassword)
    EditText cpass;

    String forgotPasswordCNIC = "";

    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;

    private void setInstances(){
        mGlobals = Globals.getInstance(this);
        SugarContext.init(this);
        ButterKnife.bind(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password_screen);
        setInstances();

        forgotPasswordCNIC = this.getIntent().getStringExtra("forgotPasswordCNIC");
    }

    public void toLogin(View view){
        if(validateFields()) {
//            updatePassword();
        }
    }

    private boolean validateFields() {
        if(pass.getText().toString().equals("")){
            pass.setError(this.getResources().getString(R.string.enter_pass));
            pass.requestFocus();
            return false;
        }
        if(pass.getText().toString().length()<8){
            pass.setError(this.getResources().getString(R.string.enter_correct_pass));
            pass.requestFocus();
            return false;
        }
        if(cpass.getText().toString().equals("")){
            cpass.setError(this.getResources().getString(R.string.re_enter_pass));
            cpass.requestFocus();
            return false;
        }
        if(cpass.getText().toString().length()<8){
            cpass.setError(this.getResources().getString(R.string.enter_correct_pass));
            cpass.requestFocus();
            return false;
        }
        if(!cpass.getText().toString().equals(pass.getText().toString())){
            cpass.setError("Confirm password does not match");
            cpass.requestFocus();
            return false;
        }
        return true;
    }

//    private void updatePassword(){
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Pleasw wait... | انتظار کریں");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//
//        JsonObject jsonObject = new JsonObject();
//        try {
//            jsonObject.addProperty("cnic", forgotPasswordCNIC);
//            jsonObject.addProperty("password", pass.getText().toString());
//            jsonObject.addProperty("password_confirmation", cpass.getText().toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Map<String, String> hashMap = new HashMap();
//        hashMap.put("data", jsonObject.toString());
//        hashMap.put("app_data", Globals.getUsage().getJSON().toString());
//
//        mAPIService.updatePassword(hashMap).enqueue(new Callback<GeneralResponse>() {
//            @Override
//            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
//                progressDialog.dismiss();
//                if (response.isSuccessful()) {
//                    Log.i("TAG", "post submitted to API." + response.body().toString());
//                    GeneralResponse loginResponse = response.body();
//                    if(loginResponse.getStatus()){
//                        // login success
//                        openSignInActivity();
//
//                    }else{
//                        SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, getString(R.string.error), loginResponse.getMessage(), null, false);
//                    }
//                } else {
//                    Globals.getUsage().showDialogFailure("Please contact admin");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralResponse> call, Throwable t) {
//                progressDialog.dismiss();
//                Log.e("failure", "Unable to submit post to API.");
//                if (t instanceof IOException) {
//                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.network_error));     // logging probably not necessary
//                }
//                else {
//                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.error_message));
//                }
//            }
//        });
//    }


    public void openSignInActivity(){
        Intent intent = new Intent(NewPasswordActivity.this, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}
