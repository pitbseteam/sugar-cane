package pk.gov.pitb.sugarcanetpv.dialogues;


import android.content.Context;
import android.view.WindowManager;

import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;

public class SweetAlertDialogs {

    private static SweetAlertDialogs instance = null;

    private SweetAlertDialogs() {
    }

    public static SweetAlertDialogs getInstance() {
        if (instance == null) {
            instance = new SweetAlertDialogs();
        }
        return instance;
    }

    public void showDialogYesNo(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
            if (title != null) {
                dialog.setTitle(title);
            }
            dialog.setContentText(message);
            dialog.setConfirmText("Yes - جی ہاں");
            dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.setCancelText("No - نہیں");
            dialog.setCancelClickListener(onClickNegativeButton);
            dialog.setCancelable(bIsCancelable);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public void showDialogOK(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, boolean bIsCancelable) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(Globals.getUsage().mContext, SweetAlertDialog.NORMAL_TYPE);

            if (title != null) {
                dialog.setTitleText(title);
            }
            if (message != null) {
                dialog.setContentText(message);
            }
            dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.setCancelable(bIsCancelable);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }
    public void showDialogPermissionDenied(Context context,OnDialogClickListener onClickPositiveButton) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);

                dialog.setTitleText("Permission Denied\nاجازت نہیں دی گئی");

                dialog.setContentText("Allow all permissions to proceed\n آگے بڑھنے کی تمام اجازت دیں");

                dialog.setConfirmClickListener(onClickPositiveButton);
                dialog.setCancelable(false);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public void showDialogOKCancel(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
            if (title != null) {
                dialog.setTitle(title);
            }
            dialog.setContentText(message);
            dialog.setCancelText("Cancel - منسوخ");
            dialog.setCancelClickListener(onClickNegativeButton);
            dialog.setConfirmText("OK - ٹھیک");
            dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.setCancelable(bIsCancelable);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public void showDialogOnBackPressed() {
        SweetAlertDialog dialog = new SweetAlertDialog(Globals.getUsage().mContext, SweetAlertDialog.WARNING_TYPE);
        dialog.setTitle("All Data will be Lost - تمام ڈیٹا کھو جائے گا");
        dialog.setContentText("Are You Sure You Want to Exit Application?\nکیا آپ واقعی ایپ بند کرنا چاھتے حو؟");
        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Globals.getUsage().mActivity.finish();
            }
        });
        dialog.setConfirmText("Yes - جی ہاں");
        dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

            }
        });
        dialog.setCancelText("No - نہیں");
        dialog.show();
    }

    public interface OnDialogClickListener extends SweetAlertDialog.OnSweetClickListener {
    }
}