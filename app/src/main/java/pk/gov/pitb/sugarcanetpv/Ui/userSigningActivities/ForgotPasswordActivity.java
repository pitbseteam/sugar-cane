package pk.gov.pitb.sugarcanetpv.Ui.userSigningActivities;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;

import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.helperClasses.Globals;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.APIService;
import pk.gov.pitb.sugarcanetpv.serverRetrofit.ApiUtils;
import pk.gov.pitb.sugarcanetpv.textWatchers.TextWatcherCNIC;

public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.txtForgotPassword_cnic)
    EditText cnic;

    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;

    private void setInstances(){
        mGlobals = Globals.getInstance(this);
        SugarContext.init(this);
        ButterKnife.bind(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_screen);
        setInstances();
        cnic.addTextChangedListener(new TextWatcherCNIC(cnic));

    }

    public void otpGenerationScreen(View view){
        if(validateField()){
//            forgotPassword();
        }
    }

    private boolean validateField() {
        if(cnic.getText().toString().equals("")){
            cnic.setError(this.getResources().getString(R.string.enter_cnic));
            cnic.requestFocus();
            return false;
        }
        if(cnic.getText().toString().length()!=15){
            cnic.setError(this.getResources().getString(R.string.enter_correct_cnic));
            cnic.requestFocus();
            return false;
        }

        return true;
    }

//    private void forgotPassword(){
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Pleasw wait... | انتظار کریں");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//
//        JsonObject jsonObject = new JsonObject();
//        try {
//            jsonObject.addProperty("cnic", cnic.getText().toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Map<String, String> hashMap = new HashMap();
//        hashMap.put("data", jsonObject.toString());
//        hashMap.put("app_data", Globals.getUsage().getJSON().toString());
//
//        mAPIService.forgotPasswordOTP(hashMap).enqueue(new Callback<GeneralResponse>() {
//            @Override
//            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
//                progressDialog.dismiss();
//                if (response.isSuccessful()) {
//                    Log.i("TAG", "post submitted to API." + response.body().toString());
//                    GeneralResponse loginResponse = response.body();
//                    if(loginResponse.getStatus()){
//                        // login success
//
//                        openSplashActivity();
//                    }else{
//                        SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, getString(R.string.error), loginResponse.getMessage(), null, false);
//                    }
//                } else {
//                    Globals.getUsage().showDialogFailure("Please contact admin");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralResponse> call, Throwable t) {
//                progressDialog.dismiss();
//                Log.e("failure", "Unable to submit post to API.");
//                if (t instanceof IOException) {
//                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.network_error));     // logging probably not necessary
//                }
//                else {
//                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.error_message));
//                }
//            }
//        });
//    }


    public void openSplashActivity(){
        /*Intent intent = new Intent(ForgotPasswordActivity.this, OTPActivity.class);
        intent.putExtra("forgotPasswordCNIC", cnic.getText().toString());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*/
    }

}
