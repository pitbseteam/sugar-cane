package pk.gov.pitb.sugarcanetpv.Ui.Fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.gov.pitb.sugarcanetpv.Adapters.FlourMillListAdapter;
import pk.gov.pitb.sugarcanetpv.R;
import pk.gov.pitb.sugarcanetpv.Ui.DrawerMainActivity;
import pk.gov.pitb.sugarcanetpv.models.syncResponse.SugarMill;

public class ShipmentListFragment extends Fragment {

    @BindView(R.id.not_available_tv)
    TextView textViewNotAvailable;

    @BindView(R.id.recycler_view_videos)
    RecyclerView recyclerView;


    FlourMillListAdapter flourMillListAdapter;
    List<SugarMill> flourMillList;
    private View parentView;

    public ShipmentListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        if(flourMillListAdapter !=null) {
            getShipmentList();
            flourMillListAdapter.updateList(flourMillList);
        }
        isEmptyList();


        super.onResume();
    }

    public boolean isEmptyList(){
        if(flourMillList==null || flourMillList.size()==0){
            textViewNotAvailable.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return true;
        }else{
            textViewNotAvailable.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            return false;
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(parentView == null){
            parentView = inflater.inflate(R.layout.fragment_farmer_list, container, false);
        }
        ButterKnife.bind(this, parentView);

        return parentView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        generateBody();

    }
    public void generateBody(){

        ((DrawerMainActivity)getActivity()).setmTextViewText("Sugar Mills List");

//        ("List of Registered Farmers");
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Registered Farmers");
        setAdapter();


        if(isEmptyList())
            ((DrawerMainActivity)getActivity()).openSaveShipmentsListingActivity();
    }

    public static Fragment newInstance()
    {
        ShipmentListFragment myFragment = new ShipmentListFragment();
        return myFragment;
    }

    private void setAdapter(){

        RecyclerView.LayoutManager lm1 = new GridLayoutManager(getActivity(), 1);
        ((GridLayoutManager) lm1).setOrientation(GridLayoutManager.VERTICAL);


        recyclerView.setLayoutManager(lm1);
        recyclerView.setHasFixedSize(true);
        getShipmentList();
        if(flourMillList ==null)
            flourMillList = new ArrayList<>();

        flourMillListAdapter = new FlourMillListAdapter(flourMillList, getActivity());
        recyclerView.setAdapter(flourMillListAdapter);
    }

    public void getShipmentList(){
        flourMillList = SugarMill.listAll(SugarMill.class);
    }

}
