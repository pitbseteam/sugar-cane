package pk.gov.pitb.sugarcanetpv.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Base64;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utilities
{

    public static byte[] getBytes(Bitmap bitmap)
    {
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm");
        String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system

        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(27);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200 ,resizedBitmap.getHeight() - 10, tPaint);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static byte[] getBytes(Bitmap resizedBitmap , String picturePath)
    {
        File file=new File(picturePath);
//    	bitmap=BitmapFactory.decodeFile(picturePath);

        //Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);//createBitmap(bitmap, 0, 0, 500, 700, matrix, true);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy  HH:mm");
        // String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
        String dateTime=sdf.format(file.lastModified());
        Canvas cs = new Canvas(resizedBitmap);
        Paint tPaint = new Paint();
        tPaint.setTextSize(27);
        tPaint.setColor(Color.RED);
        tPaint.setStyle(Style.FILL);
        cs.drawText(dateTime, resizedBitmap.getWidth() - 200 ,resizedBitmap.getHeight() - 10, tPaint);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static byte[] getSimpleBytes(Bitmap bitmap)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
    public static Bitmap getImage(byte[] image)
    {
        try {
            return BitmapFactory.decodeByteArray(image, 0, image.length);
        }catch (OutOfMemoryError e){
            e.printStackTrace();
            return null;
        }
    }
    public static Bitmap ConvertPictureToBitmap(String filePath) throws FileNotFoundException {
        FileInputStream in = new FileInputStream(filePath);
        BufferedInputStream buf = new BufferedInputStream(in);
        Bitmap bmp = BitmapFactory.decodeStream(buf);

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bmp, bmp.getWidth() / 2, bmp.getHeight() / 2, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(CompressFormat.JPEG, 100, stream);
        return resizedBitmap;
    }
    public static String bitmapToString(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
}