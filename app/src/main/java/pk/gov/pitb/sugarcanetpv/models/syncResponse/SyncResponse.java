
package pk.gov.pitb.sugarcanetpv.models.syncResponse;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SyncResponse {

    @SerializedName("code")
    private Long mCode;
    @SerializedName("data")
    private SyncData mSyncData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private Boolean mStatus;

    public Long getCode() {
        return mCode;
    }

    public void setCode(Long code) {
        mCode = code;
    }

    public SyncData getData() {
        return mSyncData;
    }

    public void setData(SyncData syncData) {
        mSyncData = syncData;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
