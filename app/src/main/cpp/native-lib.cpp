#include <jni.h>
#include <string>


extern "C"
JNIEXPORT jstring
JNICALL
Java_pk_gov_pitb_sugarcanetpv_SplashScreen_baseURL(
        JNIEnv *env,
        jobject /* this */) {
    std::string url = "https://dev.poltrain.punjabpolice.gov.pk/fdims/public/api/";
//    std::string url = "https://dfd.punjab.gov.pk/api/";
    return env->NewStringUTF(url.c_str());
}
extern "C"
JNIEXPORT jstring
JNICALL
Java_pk_gov_pitb_sugarcanetpv_SplashScreen_siteKey(
        JNIEnv *env,
        jobject /* this */) {
    std::string url = "6LePV9IUAAAAALQJrD17Y63PY_vXCA8F3szysFhG";
    return env->NewStringUTF(url.c_str());
}
