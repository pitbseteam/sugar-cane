# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep public class * implements androidx.versionedparcelable.VersionedParcelable
-keep class org.apache.** { *; }
-keep interface org.apache.** { *; }
-keep class android.support.v4.app.** { *; }
-keep class pk.gov.pitb.sugarcanetpv.models.**{*;}

-keepattributes Signature
-keepattributes Annotation
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**

-keep class retrofit2.** { *; }
-keep interface retrofit2.** { *; }
-dontwarn retrofit2.**

-dontwarn com.orm.**
-dontwarn org.apache.**
-dontwarn android.support.**
-dontwarn com.google.gms.**
-dontwarn com.google.android.gms.internal.**
-dontwarn com.google.android.gms.internal.**
-dontwarn com.google.firebase.**

-dontwarn com.nostra13.universalimageloader.**
-dontwarn com.mikhaellopez.**

-dontwarn com.squareup.retrofit2.**
-dontwarn com.google.code.gson.**
-dontwarn com.squareup.retrofit2.**
-dontwarn com.thoughtbot.**

-keep class cn.pedant.** { *; }
#-dontwarn cn.pedant.sweetalert.**
-dontwarn com.github.f0ris.sweetalert.**

-dontwarn com.github.jrvansuita.**

-dontwarn com.github.david-serrano.**

-dontwarn com.intuit.sdp.**
-dontwarn com.intuit.ssp.**

-dontwarn com.github.david-serrano.**

-dontwarn com.jaeger.statusbarutil.**

-dontwarn com.github.satyan.**
-dontwarn com.jakewharton.**

-dontwarn uk.co.chrisjenx.**
-dontwarn javax.annotation.**
-dontwarn retrofit2.Platform$Java8